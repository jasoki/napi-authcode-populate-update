require './lib/authcode'

def time_diff_milli(start, finish)
  (finish - start) * 1000.0
end

task :populate_expired, [:num, :env] do |t, args|
  num = args[:num].to_i
  env = args[:env]
  t1 = Time.now
  Authcode.populate_expired(env, num)
  t2 = Time.now
  msecs = time_diff_milli(t1, t2)
  puts msecs
end

task :remove_expired, [:env] do |t, args|
  env = args[:env]
  t1 = Time.now
  Authcode.remove_expired(env)
  t2 = Time.now
  msecs = time_diff_milli(t1, t2)
  puts msecs
end
