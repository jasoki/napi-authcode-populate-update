# NAPI Authcode Populate Update

Tests

```
$ ruby -Ilib:test test/test_authcode.rb
$ ruby -Ilib:test test/test_db.rb
$ rake populate_expired[10000,'development']
  # 10000 keys take 3 seconds
  # 1 million in 30 seconds
$ rake remove_expired['development']
```
