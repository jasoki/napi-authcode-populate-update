require 'minitest/autorun'
require_relative '../lib/index'

class TestDb < Minitest::Test
  def test_new_development
    db = Db.new('development')
    assert_equal(db.class, Db)
  end

  def test_new_load
    db = Db.new('load')
    assert_equal(db.class, Db)
  end

  def test_new_production
    db = Db.new('production')
    assert_equal(db.class, Db)
  end
end
