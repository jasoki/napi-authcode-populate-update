require 'redis'
require_relative './config'

class Db
  attr_accessor :connection
  CONFIG = Config.load

  def initialize(env)
    host = CONFIG[env]['db']['host']
    port = CONFIG[env]['db']['port']
    @connection = Redis.new(host: host, port: port)
  end
end
