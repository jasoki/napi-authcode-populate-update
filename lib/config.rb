require "json"

class Config
  APP_CONFIGS_PATH = "configs/app_configs.json"

  def self.load
    json_location = File.join(Dir.pwd, APP_CONFIGS_PATH)
    json_data = File.read(json_location)
    JSON.parse(json_data)
  end
end
