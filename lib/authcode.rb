require 'uuid'
require_relative './db'

class Authcode
  EXPIRATION = 86400000

  def self.populate_expired(env, num)
    connection = Db.new(env).connection

    num.times do
      key = self.redis_key
      time_now = Time.now.to_i * 1000
      expires_at = time_now + EXPIRATION
      connection.hmset(key, 'id', key,
                            'lastName', 'Ster',
                            'guid', 'D877082A5CBC5AC7E040960A390313EF',
                            'catalog', '101',
                            'cocat', '40134:101:en_US',
                            'expiresAt', expires_at,
                            'clientId', 'napi_populate_fake',
                            'firstName', 'Nap',
                            'responseType', 'code',
                            'rat', 'rat',
                            'createdAt', time_now,
                            'type', 'authcode',
                            'username', 'faker')
    end
  end

  def self.remove_expired(env)
    connection = Db.new(env).connection

    pager = 0
    loop do
      scanned = connection.scan(pager, match: 'AuthCode:*', count: 500)
      scanned[1].each do |key|
        connection.del(key) if self.expired?(key, connection)
      end
      pager = scanned[0].to_i
      break if pager === 0
    end
  end

  # (Time.now.to_i + EXPIRATION) * 10000 for testing
  def self.expired?(key, connection)
    return false if !connection.hgetall(key)
    return true if !connection.hgetall(key)['expiresAt']
    connection.hgetall(key)['expiresAt'].to_i < Time.now.to_i * 1000
  end

  def self.redis_key
    uuid = UUID.new
    "AuthCode:#{uuid.generate}"
  end
end
